'use strict';

module.exports = {
  async up(queryInterface, Sequelize) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('Users', [{
      role_id: "3",
      name: 'Super Admin',
      email: 'SuperAdmin@gmail.com',
      encryptedPassword: "kepoyaaa",
      createdAt: "2022-05-11 21:49:18.254+07",
      updatedAt: "2022-05-11 21:49:18.254+07"
      // isBetaMember: false
    }], {});
  },

  async down(queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
