const express = require("express");
const controllers = require("../app/controllers");
const path = require("path");
const multer = require("multer");
const apiRouter = express.Router();

const appe = express();


apiRouter.use(express.static(__dirname + "./public/"))
appe.use(express.json())
const PUBLIC_DIRECTORY = path.join(__dirname, "./public");

// Set format request
appe.use(express.urlencoded({ extended: true }));

// Set PUBLIC_DIRECTORY sebagai
// static files di express
appe.use(express.static(PUBLIC_DIRECTORY));
// appe.use('/public/upload', express.static(__dirname + '/public/upload'))

/**
 * Authentication Resource
 * */
apiRouter.get(
  "/api/v1/whoami",
  controllers.api.v1.auth.authorize,
  controllers.api.v1.auth.whoAmI
);
apiRouter.post("/api/v1/login", controllers.api.v1.auth.login);
apiRouter.post("/api/v1/register", controllers.api.v1.auth.register);


// user
apiRouter.post("/user/register", controllers.api.v1.auth.register);
// short
apiRouter.get("/user/home", controllers.api.v1.user.home);
apiRouter.get("/user/findCar", controllers.api.v1.user.findCar);
// end user

const diskStorage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, path.join(__basedir + "/public"));
  },
  filename: (req, file, cb) => {
    cb(null, file.fieldname + "-" + Date.now() + path.extname(file.originalname));
  },
});
const uploadFile = multer({ storage: diskStorage });
/**
 * Authentication Resource
 * */
// apiRouter.get(
//   "/api/v1/whoami",
//   controllers.api.v1.authController.authorize,
//   controllers.api.v1.authController.whoAmI
// );
// apiRouter.post("/api/v1/login", controllers.api.v1.authController.login);
// apiRouter.post("/api/v1/register", controllers.api.v1.authController.register);

// short
apiRouter.get("/short/:id/cari", controllers.api.v1.car.short)
// get all cars
apiRouter.get("/", controllers.api.v1.car.list)
apiRouter.get("/cars", controllers.api.v1.car.list)
// GET /cars/create dari file create.ejs
apiRouter.get("/cars/formAdd", controllers.api.v1.car.formAdd);
//Create a new car
apiRouter.post("/cars/add", uploadFile.single("photo"), controllers.api.v1.car.add);
// get form update
apiRouter.get("/cars/:id/update", controllers.api.v1.car.selectCar);
// update a car
apiRouter.post("/cars/update/:id", uploadFile.single("photo"), controllers.api.v1.car.updateCar);
// Delete car by ID
apiRouter.get('/cars/delete/:id', controllers.api.v1.car.deleteCar);

apiRouter.use(controllers.api.main.onLost);
apiRouter.use(controllers.api.main.onError);

module.exports = apiRouter;


