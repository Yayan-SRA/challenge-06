'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class roleUser extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      roleUser.hasMany(models.User, {
        as: 'User',
        foreignKey: 'role_id'
      })
    }
  }
  roleUser.init({
    role: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'roleUser',
  });
  return roleUser;
};