'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Cars extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      Cars.belongsTo(models.Size, {
        foreignKey: 'size_id'
      })
    }
  }
  Cars.init({
    size_id: DataTypes.INTEGER,
    name: DataTypes.STRING,
    transmission: DataTypes.STRING,
    rentPerDay: DataTypes.INTEGER,
    passegger: DataTypes.STRING,
    year: DataTypes.STRING,
    desc: DataTypes.STRING,
    photo: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Cars',
  });
  return Cars;
};