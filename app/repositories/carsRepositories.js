const { Cars, Ukur } = require("../models");

module.exports = {
    short({ size_id }) {
        console.log("coba lihat size_id", size_id)
        return Cars.findAll({
            order: ['id'],
            where: { size_id }
        });
    },
    findAll() {
        return Cars.findAll({
            order: ['id']
        });
    },
    findAllUkur() {
        return Ukur.findAll({
            order: ['id']
        });
    },
    create(createBody) {
        return Cars.create(createBody);
    },
    findOneCar({ id }) {
        return Cars.findOne({
            where: { id: id },
            include: [{
                model: Ukur
            }]
        });
    },
    updateCar({ id }, updateBody) {
        return Cars.update(updateBody, { where: { id } });
    },
    deleteCar({ id }) {
        console.log("coba lihat id repo", id)
        const hapus = Cars.destroy({ where: { id } })
        console.log("coba lihat hapus", hapus)
        return hapus;
    }
};