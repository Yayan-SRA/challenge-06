const express = require("express");
const morgan = require("morgan");
const router = require("../config/routes");
const path = require("path");

// app.use(express.json());
const app = express();
// const bodyParser = require('body-parser');
/** Install JSON request parser */
app.use(express.json());
// app.use(bodyParser.urlencoded({ extended: true }));
// app.use(bodyParser.json());
app.use(express.urlencoded({ extended: true }))
/** Install request logger */
app.use(morgan("dev"));


// Path ke directory public
// Yang bakal kita jadikan public
// Sehingga user bisa akses CSS dan Javascript
// Di browser
// global.__basedir = __dirname;
// var base_path = __basedir
const PUBLIC_DIRECTORY = path.join(__dirname, "../", "./public");
// console.log(base_path);
app.use(express.static(PUBLIC_DIRECTORY));
console.log(PUBLIC_DIRECTORY);

app.set("view engine", "ejs");

/** Install Router */
app.use(router);

module.exports = app;
