const car = require("./carsController");
const user = require("./usersController");
const auth = require("./authController");
module.exports = {
  car, user, auth
};