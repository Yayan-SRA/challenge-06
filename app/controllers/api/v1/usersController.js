// const carsService = require("../../../services/carsServices");

module.exports = {
    home(req, res) {
        res.render("users/index",)
    },

    findCar(req, res) {
        res.render("users/carimobil")
    },

    formAdd(req, res) {
        carsService
            .listUkur()
            .then((ukur) => {
                console.log(ukur)
                res.render("addcar", { ukur });
            });
    },

    add(req, res) {
        carsService
            .create({
                size_id: req.body.size_id,
                name: req.body.name,
                rentPerDay: req.body.rentPerDay,
                photo: req.file.filename,
            })
            .then((car) => {
                console.log(car);
                res.send(
                    '<script>window.location.href="/";document.getElementById("alert-save").click();</script>'
                );
            })
            .catch((err) => {
                res.status(422).json({
                    status: "FAIL",
                    message: err.message,
                });
            });
    },

    async selectCar(req, res) {
        const id = req.params.id;
        const cars = await carsService.oneCar({
            id
        })
        // console.log("get id", req.params.id);
        // console.log("hasil id", id);

        const coba = await carsService.listUkur()
        console.log("coba", coba)
        console.log("cars", cars)
        res.render("edit", { cars, coba });
    },

    updateCar(req, res) {
        const id = req.params;
        carsService.updateCar({ id }, {
            size_id: req.body.size_id,
            name: req.body.name,
            rentPerDay: req.body.rentPerDay,
            photo: req.file.filename,
        }).then(() => {
            res.redirect("/");
        }).catch(err => {
            res.status(422).json("Can't update car")
        })
    },

    deleteCar(req, res) {
        const id = req.params.id;
        console.log("coba lihat id", id)
        carsService.deleteCar({ id }).then(() => {
            res.redirect("/");
        }).catch(err => {
            res.status(422).json("Can't delete car")
        })
    }

};